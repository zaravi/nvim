{
  globals = { 
    mapleader = " ";
    maplocalleader = " ";
  };

  keymaps = [
    { mode = "n"; key = "u"; action = "i"; options = { silent = true; }; }
    { mode = "n"; key = "l"; action = "u"; options = { silent = true; }; }

    { mode = "n"; key = "m"; action = "h"; options = { silent = true; }; }
    { mode = "v"; key = "m"; action = "h"; options = { silent = true; }; }
    { mode = "n"; key = "n"; action = "j"; options = { silent = true; }; }
    { mode = "v"; key = "n"; action = "j"; options = { silent = true; }; }
    { mode = "n"; key = "e"; action = "k"; options = { silent = true; }; }
    { mode = "v"; key = "e"; action = "k"; options = { silent = true; }; }
    { mode = "n"; key = "i"; action = "l"; options = { silent = true; }; }
    { mode = "v"; key = "i"; action = "l"; options = { silent = true; }; }

    { mode = "n"; key = "k"; action = "n"; options = { silent = true; }; }
    { mode = "n"; key = "K"; action = "N"; options = { silent = true; }; }

    { mode = "i"; key = "ii"; action = "<Esc>"; }
  ];
}
