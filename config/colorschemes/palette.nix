{
  colorschemes.palette = { 
    enable = true;
    settings = {
      italics = true;
      transparent_background = true;
      palettes = { 
        accent = "wildcosmic";
        main = "wildcosmic";
        state = "wildcosmic";
      };
      custom_palettes = {
        accent = {
          wildcosmic = {
            accent0 = "#F62A63";
            accent1 = "#D4AC77";
            accent2 = "#D4AC77";
            accent3 = "#57C5ED";
            accent4 = "#57C5ED";
            accent5 = "#A572FD";
            accent6 = "#A572FD";
          };
        };
        main = {
          wildcosmic = {
            color0 = "#262626";
            color1 = "#262626";
            color2 = "#d4ac77";
            color3 = "#4A4949"; # numbers
            color4 = "#d4ac77"; # Text
            color5 = "#fffafa"; # Text fn
            color6 = "#57c5ed"; # value / comment
            color7 = "#f62a63"; # value
            color8 = "#A572FD"; # lower text, accents
          };
        };
        state = {
          wildcosmic = {
            error = "#F62a63";
            warning = "#D4AC77";
            hint = "#57C5ED";
            ok = "#A572FD";
            info = "#57C5ED";
          };
        };
      };
    };
  };
}
