{
  plugins.lsp = {
    enable = true;
    servers = {
      rust-analyzer = {
        enable = true;
        installCargo = true;
        installRustc = true;
        autostart = true;
        settings = {
          numThreads = 16;
          runnables.extraArgs = ["--release"];
        };
      };
      lua-ls.enable = true;
    };
  };
}
